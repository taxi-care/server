const driverLocationService = require("../Services/driverLocationService");
class DriverLocationController {
  updateLocation = async (req, res, next) => {
    try {
      return res
        .status(201)
        .json(await driverLocationService.saveLocation(req.body));
    } catch (error) {
      console.log("Error save location driver");
      next(error);
    }
  };

  nearbyDrivers = async (req, res, next) => {
    try {
      if (!req.body || !req.body.userCoordinates) {
        return res.status(400).json({
          message: "Missing required parameters in the request body.",
        });
      }

      const drivers = await driverLocationService.nearbyDrivers(req.body);

      if (!drivers) {
        return res.status(404).json({ message: "No drivers found nearby." });
      }

      return res.status(200).json(drivers);
    } catch (error) {
      console.error("Error fetching nearby drivers:", error.message);
      next(error);
    }
  };

  findDriverLocationbyId = async (req, res, next) => {
    try {
      const driver = await driverLocationService.findDriverLocationbyId(
        req.params.driverId
      );
      res.status(200).json(driver);
    } catch (error) {
      res.status(500).json(error);
    }
  };
}

module.exports = new DriverLocationController();

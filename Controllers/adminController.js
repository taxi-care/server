const AdminService = require('../Services/adminService');

class AdminController {
    getAdminTrip = async (req, res, next) => {
        try {
            return res
                .status(201)
                .json(await AdminService.aggregateTrip());
        } catch (error) {
            console.log("Error get admin trip");
            next(error);
        }
    }
    getRideStatistics = async (req, res, next) => {
        try {
            return res
                .status(201)
                .json(await AdminService.aggregateRideStatistics());
        } catch (error) {
            console.log("Error get rides statics");
            next(error);
        }
    }
    getTopDrivers = async (req, res, next) => {
        try {
            return res
                .status(201)
                .json(await AdminService.getTopDriver());
        } catch (error) {
            console.log("Error get top driver");
            next(error);
        }
    }
}

module.exports = new AdminController()
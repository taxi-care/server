const validator = require("validator");
const userService = require("../Services/userService");
const registerUser = async (req, res) => {
  try {
    const { name, password, phone, role } = req.body;
    if (!role) {
      req.body.role = "customer";
    }
    //Validate
    if (!name || !phone || !password) {
      return res.status(400).json("All fields are required");
    }
    if (!validator.isMobilePhone(phone)) {
      return res.status(400).json("Phone must be valid phone");
    }
    if (!validator.isStrongPassword(password)) {
      return res.status(400).json("Password must be strong password");
    }
    if (role == "driver") {
      if (!req.body.license_plate || !req.body.car_model_id) {
        return res.status(400).json("All fields are required");
      }
    }
    //Call service user
    const user = await userService.registerUser(req.body);
    return res.status(200).json(user);
  } catch (error) {
    return res.status(500).json(error);
  }
};

const loginUser = async (req, res) => {
  try {
    const { phone, password } = req.body;
    const role = req.params.role;
    if (!phone || !password || !role)
      return res.status(400).json("All fields are required");
    req.body.role = role;
    const result = await userService.loginUser(req.body);
    return res.status(200).json(result);
  } catch (error) {
    return res.status(500).json(error);
  }
};

const refreshToken = async (req, res) => {
  if (!req.body.refreshToken) {
    return res.status(404).json("Refresh token are required");
  }
  try {
    const response = await userService.refreshToken(req.body.refreshToken);
    return res.status(200).json(response);
  } catch (error) {
    return res.status(500).json(error);
  }
};

const findUser = async (req, res) => {
  const userId = req.params.userId;
  try {
    //Call services
    const result = await userService.findUser(userId);
    return res.status(200).json(result);
  } catch (error) {
    return res.status(500).json(error);
  }
};

const getCurrentUser = async (req, res) => {
  try {
    const { _id } = req.user;
    const result = await userService.findUser(_id);
    return res.status(200).json(result);
  } catch (error) {
    return res.status(500).json(error);
  }
};

module.exports = {
  registerUser,
  loginUser,
  findUser,
  getCurrentUser,
  refreshToken,
};

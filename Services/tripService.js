const tripModel = require("../Models/tripModel");

const createTrip = async ({
  source,
  sourceCoordinates,
  destination,
  destinationCoordinates,
  PhoneCustomer,
  NameCustomer,
  distance,
  price,
  userId,
  selectedCar,
}) => {
  try {
    let trip = new tripModel({
      source,
      sourceCoordinates,
      destination,
      destinationCoordinates,
      PhoneCustomer,
      NameCustomer,
      distance,
      price,
      userId,
      selectedCar,
    });
    await trip.save();
    return {
      code: 200,
      trip,
    };
  } catch (error) {
    return {
      code: 500,
      message: error.message,
      status: "error",
    };
  }
};

const findUserHaveTrip = async (body) => {
  try {
    const trips = await tripModel.find({
      ...body,
      status: { $nin: ["complete", "cancel"] },
    });
    return {
      code: 200,
      trips,
    };
  } catch (error) {
    return {
      code: 500,
      message: error.message,
      status: "error",
    };
  }
};

const updateTripStatus = async ({ tripId, newStatus }) => {
  try {
    const trip = await tripModel.findById(tripId);
    if (!trip) {
      return {
        code: 404,
        message: "Cuộc đi chuyển không tồn tại.",
        status: "error",
      };
    }
    trip.status = newStatus;
    await trip.save();

    return {
      code: 200,
      trip,
    };
  } catch (error) {
    return {
      code: 500,
      message: error.message,
      status: "error",
    };
  }
};
const acceptTrip = async ({ tripId, driverId }) => {
  try {
    const trip = await tripModel.findOne({ _id: tripId, status: "pending" });
    if (!trip) {
      return {
        code: 404,
        message: "Đã có tài xế nhận chuyến đi hoặc chuyến đi đã bị hủy",
        status: "error",
      };
    } else {
      const result = await tripModel.findOneAndUpdate(
        { _id: tripId, status: "pending" },
        { status: "intrip", driverId: driverId },
        { new: true }
      );

      return {
        code: 200,
        metadata: result,
      };
    }
  } catch (error) {
    return {
      code: 500,
      message: error.message,
      status: "error",
    };
  }
};

const getUserTrips = async (body) => {
  try {
    const trips = await tripModel
      .find(body)
      .sort({ createdAt: -1 }) // Sắp xếp theo thời gian giảm dần (mới nhất trước)
      .exec();
    return {
      code: 200,
      metadata: trips,
    };
  } catch (error) {
    return {
      code: 500,
      message: error.message,
      status: "error",
    };
  }
};

const DriverCancelTrip = async ({ tripId }) => {
  try {
    const result = await tripModel.findOneAndUpdate(
      { _id: tripId, status: "intrip" },
      { status: "pending", driverId: null },
      { new: true }
    );
    if (!result) {
      throw new Error("Not found trip to cancel");
    }
    return {
      code: 200,
      metadata: result,
    };
  } catch (error) {
    return {
      code: 500,
      message: "Lỗi nằm ở đây nhé",
      status: "error",
    };
  }
};

module.exports = {
  createTrip,
  findUserHaveTrip,
  updateTripStatus,
  acceptTrip,
  getUserTrips,
  DriverCancelTrip,
};

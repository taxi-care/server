class SocketService {
  //connection socket
  static onlineUsers = [];
  static onlineDrivers = [];

  static connection(socket) {
    //Listen to disconnect socket
    socket.on("disconnect", () => {
      SocketService.onlineUsers = SocketService.onlineUsers.filter(
        (user) => user.socketId !== socket.id
      );
      SocketService.onlineDrivers = SocketService.onlineDrivers.filter(
        (user) => user.socketId !== socket.id
      );
      console.log(
        "-------Danh sách User khi có một User Disconnect socket----------"
      );
      console.log("Users Online ::");
      console.log(SocketService.onlineUsers);
      console.log("Drivers Online ::");
      console.log(SocketService.onlineDrivers);
      console.log("-------------------------------------------");
    });
    //listen to a new connection
    socket.on("addNewUser", (req) => {
      if (req.userId) {
        if (
          req.role == "customer" &&
          !SocketService.onlineUsers.some((user) => user.userId === req.userId)
        ) {
          SocketService.onlineUsers.push({
            ...req,
            socketId: socket.id,
          });
        } else if (
          req.role == "driver" &&
          !SocketService.onlineDrivers.some(
            (user) => user.userId === req.userId
          )
        ) {
          SocketService.onlineDrivers.push({
            ...req,
            socketId: socket.id,
          });
        }
      }
      console.log(
        "--------Danh sách User khi có một User mới online----------"
      );
      console.log("Users Online ::");
      console.log(SocketService.onlineUsers);
      console.log("Drivers Online ::");
      console.log(SocketService.onlineDrivers);
      console.log("-------------------------------------------");
    });
    //listen on update status driver online
    socket.on("updateStatusDriver", (req) => {
      SocketService.onlineDrivers.forEach((driver) => {
        if (driver.userId === req.userId) {
          driver.status = req.status;
        }
      });
      console.log(
        "--------Danh sách User khi có một Driver Cập Nhật status----------"
      );
      console.log("Users Online ::");
      console.log(SocketService.onlineUsers);
      console.log("Drivers Online ::");
      console.log(SocketService.onlineDrivers);
      console.log("-------------------------------------------");
    });

    //Listen driver Update location and emit to customer
    socket.on("locationDriverInTrip", (req) => {
      const socketCustomer = SocketService.onlineUsers.find(
        (user) => user.userId === req.userId
      );
      _io
        .to(socketCustomer?.socketId)
        .emit("locationDriverInTrip", req.driverLocation);
    });
  }
}

module.exports = SocketService;

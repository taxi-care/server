const express = require("express");
const router = express.Router();
const {
  registerUser,
  loginUser,
  findUser,
  getCurrentUser,
  refreshToken,
} = require("../Controllers/userController");
const verifyToken = require("../middlewares/verify_token");

router.post("/register", registerUser);
router.post("/login/:role", loginUser);
router.get("/find/:userId", findUser);
router.post("/refresh-token", refreshToken);

router.use(verifyToken);
router.get("/", getCurrentUser);
module.exports = router;

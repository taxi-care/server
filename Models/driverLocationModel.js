"use strict";
const { model, Schema, Types } = require("mongoose");

const DOCUMENT_NAME = "driverLocation";
const COLLECTION_NAME = "driverLocations";

// Declare the Schema of the Mongo model
var driverLocationSchema = new Schema(
  {
    driverId: {
      type: Schema.Types.ObjectId,
      required: true,
    },
    currentLocation: {
      type: { type: String, default: "Point" },
      coordinates: { type: [Number], required: true },
    },
    carModelId: {
      type: String,
      required: true,
    },
    licensePlate: {
      type: String,
      required: true,
    },
  },
  {
    timestamps: true,
    collection: COLLECTION_NAME,
  }
);

driverLocationSchema.index({ currentLocation: "2dsphere" });
//Export the model
module.exports = model(DOCUMENT_NAME, driverLocationSchema);

"use strict";

// const axios = require("axios");
const driverLocationModel = require("../Models/driverLocationModel");
const { getInfoData } = require("../utils");
const maxDistance = 10000;

const accessTokenMapbox =
  "pk.eyJ1IjoibGFtZHUxMjUiLCJhIjoiY2xseG9xaDBkMWN4aTNpbzl4Y3VvOHU5cSJ9.25lpKlLI5Yo1ZCafisok0g";

class DriverLocationService {
  static saveLocation = async ({
    driverId,
    coordinates,
    carModelId,
    licensePlate,
  }) => {
    try {
      const existDriver = await driverLocationModel
        .findOne({ driverId })
        .lean();
      if (existDriver) {
        return await driverLocationModel.findOneAndUpdate(
          { driverId: driverId },
          {
            $set: {
              "currentLocation.coordinates": coordinates,
              carModelId,
              licensePlate,
            },
          },
          { new: true }
        );
      }
      const newLocation = await driverLocationModel.create({
        driverId,
        currentLocation: {
          type: "Point",
          coordinates,
        },
        carModelId,
        licensePlate,
      });
      if (newLocation) {
        return {
          code: 201,
          metadata: {
            location: getInfoData({
              fields: [
                "driverId",
                "currentLocation",
                "carModelId",
                "licensePlate",
              ],
              object: newLocation,
            }),
          },
        };
      }
      return {
        code: 201,
        metadata: null,
      };
    } catch (error) {
      return {
        code: "501",
        message: error.message,
        status: "error",
      };
    }
  };
  static nearbyDrivers = async ({ userCoordinates, carModelId }) => {
    try {
      const drivers = await driverLocationModel.find({
        carModelId,
        currentLocation: {
          $near: {
            $geometry: {
              type: "Point",
              coordinates: userCoordinates,
            },
            $maxDistance: maxDistance,
          },
        },
      });

      if (drivers.length) {
        return {
          code: 200,
          metadata: {
            drivers: drivers.map((driver) =>
              getInfoData({
                fields: ["driverId", "currentLocation"],
                object: driver,
              })
            ),
          },
        };
      }
      return {
        code: 200,
        metadata: null,
      };
    } catch (error) {
      return {
        code: "501",
        message: error.message,
        status: "error",
      };
    }
  };

  static findDriverLocationbyId = async (driverId) => {
    try {
      const driver = await driverLocationModel.findOne({ driverId }).lean();
      if (!driver) {
        return {
          code: 404,
          message: "Không tìm thấy Driver Location",
          status: "error",
        };
      }
      return { code: 200, metadata: driver };
    } catch (error) {
      return { code: 500, message: error.message, status: "error" };
    }
  };
}

module.exports = DriverLocationService;

const express = require("express");
const router = express.Router();
const {
  createTrip,
  findUserHaveTrip,
  updateTripStatus,
  acceptTrip,
  getUserTrips,
  driverCancelTrip,
} = require("../Controllers/tripController");

router.post("/create", createTrip);
router.post("/user-have-trip", findUserHaveTrip);
router.post("/update-status", updateTripStatus);
router.post("/accept", acceptTrip);
router.post("/get-all-trip", getUserTrips);
router.post("/driver-cancel", driverCancelTrip);

module.exports = router;

const carModel = require("../Models/carModel");

const createCar = async (model_name, model_description, price) => {
  try {
    let car = new carModel({ model_name, model_description, price });
    await car.save();
    return {
      code: 200,
      carId: car._id,
      model_name,
      model_description,
      price,
    };
  } catch (error) {
    return {
      code: 500,
      message: error.message,
      status: "error",
    };
  }
};

const getCars = async () => {
  try {
    let cars = await carModel.find();
    return {
      code: 200,
      cars,
    };
  } catch (error) {
    return {
      code: 500,
      message: error.message,
      status: "error",
    };
  }
};

const getCarById = async ({ carId }) => {
  try {
    // Import the 'carModel' from wherever it is defined
    const car = await carModel.findById(carId); // Sử dụng carId để truy vấn xe theo ID

    if (!car) {
      // Xử lý trường hợp không tìm thấy xe với ID cung cấp
      throw new Error("Không tìm thấy xe với ID đã cho");
    }

    return {
      code: 200,
      metadata: car,
    };
  } catch (error) {
    return {
      code: 500,
      message: error.message,
      status: "error",
    };
  }
};
module.exports = { createCar, getCars, getCarById };

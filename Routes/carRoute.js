const express = require("express");
const router = express.Router();
const {
  createCar,
  getCars,
  getCarById,
} = require("../Controllers/carController");

router.post("/create", createCar);
router.get("/:carId", getCarById);
router.get("/", getCars);

module.exports = router;

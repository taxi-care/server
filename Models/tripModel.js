const mongoose = require("mongoose");

const tripSchema = new mongoose.Schema(
  {
    source: {
      type: String,
      required: true,
    },
    sourceCoordinates: {
      type: [Number],
      required: true,
    },
    destination: {
      type: String,
      required: true,
    },
    destinationCoordinates: {
      type: [Number],
      required: true,
    },
    PhoneCustomer: {
      type: String,
      required: true,
    },
    NameCustomer: {
      type: String,
      required: true,
    },
    distance: {
      type: Number,
      default: 0,
    },
    price: {
      type: Number,
      default: 0,
    },
    userId: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
    },
    selectedCar: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
    },
    driverId: {
      type: mongoose.Schema.Types.ObjectId,
      default: null,
    },
    status: {
      type: String,
      enum: ["pending", "intrip", "complete", "cancel"],
      default: "pending",
    },
  },
  {
    timestamps: true,
  }
);

// Tạo model từ schema
const Trip = mongoose.model("Trip", tripSchema);

module.exports = Trip;

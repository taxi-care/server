const express = require("express");
const app = express();
const server = require("http").createServer(app);
const io = require("socket.io")(server, {
  cors: {
    origin: "*",
  },
});
global._io = io;
const cors = require("cors");
require("dotenv").config;
const SocketService = require("./Services/socketService");

require("dotenv").config();
const userRouter = require("./Routes/userRoute");
const carRouter = require("./Routes/carRoute");
const driverLocationRouter = require("./Routes/driverLocationRoute");
const tripRouter = require("./Routes/tripRoute");
const adminRouter = require("./Routes/adminRoute");

//Connect database
require("./config/db");

app.use(express.json());
// Sử dụng middleware cors
app.use(cors());
//Router
app.use("/api/users", userRouter);
app.use("/api/cars", carRouter);
app.use("/api/driver-location", driverLocationRouter);
app.use("/api/trips", tripRouter);
app.use("/api/admin", adminRouter);

const port = process.env.PORT || 8000;

global._io.on("connection", SocketService.connection);

server.listen(port);

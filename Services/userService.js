const userModel = require("../Models/userModel");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

const hashPassword = (password) =>
  bcrypt.hashSync(password, bcrypt.genSaltSync(3));

const registerUser = async (body) => {
  try {
    let user = await userModel.findOne({ phone: body.phone, role: body.role });
    if (user) {
      throw new Error("User with the given phone already exists");
    }
    user = new userModel(body);
    user.password = hashPassword(user.password);
    await user.save();

    // Tạo refresh_token
    const refreshToken = jwt.sign(
      { _id: user._id },
      process.env.JWT_SECRET_REFRESH_TOKEN,
      {
        expiresIn: "5d",
      }
    );

    // Cập nhật refresh_token trong bản ghi của người dùng
    user.refresh_token = refreshToken;
    await user.save();

    const accessToken = jwt.sign({ user }, process.env.JWT_SECRET, {
      expiresIn: "30s",
    });

    return {
      code: 200,
      metadata: {
        access_token: `Bearer ${accessToken}`,
        refresh_token: refreshToken,
      },
    };
  } catch (error) {
    return {
      code: 501,
      message: error.message,
      status: "error",
    };
  }
};

const loginUser = async ({ phone, password, role }) => {
  try {
    let user = await userModel.findOne({ phone, role });
    if (!user) {
      throw new Error("Invalid email or password...");
    }
    const isValidPassword = bcrypt.compareSync(password, user.password);
    if (!isValidPassword) {
      throw new Error("Invalid email or password...");
    }
    // Tạo refresh_token
    const refreshToken = jwt.sign(
      { _id: user._id },
      process.env.JWT_SECRET_REFRESH_TOKEN,
      {
        expiresIn: "5d",
      }
    );
    // Cập nhật refresh_token trong bản ghi của người dùng
    user.refresh_token = refreshToken;
    await user.save();
    const accessToken = jwt.sign({ user }, process.env.JWT_SECRET, {
      expiresIn: "30s",
    });
    return {
      code: 200,
      metadata: {
        access_token: `Bearer ${accessToken}`,
        refresh_token: refreshToken,
      },
    };
  } catch (error) {
    return {
      code: 501,
      message: error.message,
      status: "error",
    };
  }
};

const refreshToken = async (refreshToken) => {
  try {
    let user = await userModel.findOne({ refresh_token: refreshToken });
    if (user) {
      const decode = jwt.verify(
        refreshToken,
        process.env.JWT_SECRET_REFRESH_TOKEN,
        (err) => {
          if (err) {
            throw new Error(
              "Refresh Token is invalid or expired. Require Login"
            );
          }
        }
      );
      const accessToken = jwt.sign({ user }, process.env.JWT_SECRET, {
        expiresIn: "30s",
      });
      return {
        code: 200,
        metadata: {
          access_token: `Bearer ${accessToken}`,
          refresh_token: refreshToken,
        },
      };
    }
  } catch (error) {
    return {
      code: 401,
      status: "error",
      message: err,
    };
  }
};

const findUser = async (userId) => {
  try {
    const user = await userModel.findById(userId);
    if (!user) {
      throw new Error("No user found");
    }
    return { code: 200, metadata: user };
  } catch (error) {
    return {
      code: 501,
      message: error.message,
      status: "error",
    };
  }
};

module.exports = { registerUser, loginUser, findUser, refreshToken };

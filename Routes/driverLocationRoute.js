const express = require("express");
const driverLocationController = require("../Controllers/driverLocationController");
const router = express.Router();

//updateLocation
router.post("/update", driverLocationController.updateLocation);
// //findDriver
// router.get("/nearby-drivers", driverLocationController.nearbyDrivers);
//findDriverById
router.get("/:driverId", driverLocationController.findDriverLocationbyId);

module.exports = router;

"use strict";
const mongoose = require("mongoose");

class Database {
  constructor() {
    this.connect();
  }

  connect(type = "mongodb") {
    if (1 == 1) {
      mongoose;
    }
    mongoose
      .connect(`${process.env.CONNECT_DB}`, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
      })
      .then((_) => console.log("Connect mongodb success"))
      .catch((err) => console.log("Error connect!"));
  }

  static getInstance() {
    if (!Database.instance) {
      Database.instance = new Database();
    }
    return Database.instance;
  }
}

const instanceMongodb = Database.getInstance();
module.exports = instanceMongodb;

const tripService = require("../Services/tripService");
const driverLocationService = require("../Services/driverLocationService");
const ServiceSocket = require("../Services/socketService");

const createTrip = async (req, res) => {
  try {
    const trip = await tripService.createTrip(req.body);
    const driverNearData = await driverLocationService.nearbyDrivers({
      userCoordinates: req.body.sourceCoordinates,
      carModelId: req.body.selectedCar,
    });

    console.log("Danh sách driver online khi tạo trip......");
    console.log(ServiceSocket.onlineDrivers);
    console.log("------------------------------------------------------");
    let driverNearList = [];
    if (driverNearData.metadata && driverNearData?.code < 400) {
      if (driverNearData.metadata.drivers.length) {
        driverNearList = driverNearData.metadata.drivers;
        driverReceiveNoti = ServiceSocket.onlineDrivers.filter((driver) =>
          driverNearList.some(
            (item) =>
              driver.userId === item.driverId.toString() &&
              driver.status == "active"
          )
        );
        console.log("danh sách driver sẽ nhận thông báo");
        console.log(driverReceiveNoti);
        // Gửi thông báo tới các driver qua Socket.io
        driverReceiveNoti.forEach((driver) => {
          _io
            .to(driver.socketId)
            .emit("newTrip", { tripId: trip._id, ...trip });
        });
      }
    }
    return res.status(200).json(trip);
  } catch (error) {
    return res.status(500).json(error);
  }
};

const findUserHaveTrip = async (req, res) => {
  try {
    const tripNotEnd = await tripService.findUserHaveTrip(req.body);
    return res.status(200).json(tripNotEnd);
  } catch (error) {
    return res.status(500).json(error);
  }
};

const updateTripStatus = async (req, res) => {
  try {
    const updateTrip = await tripService.updateTripStatus(req.body);
    if (updateTrip.code < 400 && updateTrip.trip.status == "complete") {
      const socketCustomer = ServiceSocket.onlineUsers.find(
        (user) => user.userId === updateTrip.trip.userId.toString()
      );
      _io.to(socketCustomer?.socketId).emit("completedTrip", updateTrip);
    }
    return res.status(200).json(updateTrip);
  } catch (error) {
    return res.status(500).json(error);
  }
};

const acceptTrip = async (req, res) => {
  try {
    const acceptTrip = await tripService.acceptTrip(req.body);
    if (acceptTrip.code < 400) {
      socketCustomer = ServiceSocket.onlineUsers.find(
        (user) => user.userId === acceptTrip.metadata.userId.toString()
      );
      console.log("User sẽ được gửi thông báo", socketCustomer);
      _io.to(socketCustomer?.socketId).emit("acceptTrip", acceptTrip);
    }
    return res.status(200).json(acceptTrip);
  } catch (error) {
    return res.status(500).json(error);
  }
};

const getUserTrips = async (req, res) => {
  try {
    const trips = await tripService.getUserTrips(req.body);
    return res.status(200).json(trips);
  } catch (error) {
    return res.status(500).json(error);
  }
};

const driverCancelTrip = async (req, res) => {
  try {
    if (!req.body.tripId && !req.body.driverId) {
      throw new Error("tripId or driverId are required");
    }
    const trip = await tripService.DriverCancelTrip(req.body);
    if (trip?.code < 400 && trip?.metadata.status == "pending") {
      //Gửi thông báo real time tới cho customer là tài xế đã hủy chuyến
      const socketCustomer = ServiceSocket.onlineUsers.find(
        (user) => user.userId === trip.metadata.userId.toString()
      );
      _io.to(socketCustomer?.socketId).emit("driverCancelTrip", trip.metadata);
      //Tìm các tài xế thích hợp với chuyến đi loại trừ tài xế vừa hủy chuyến
      const driverNearData = await driverLocationService.nearbyDrivers({
        userCoordinates: trip.metadata.sourceCoordinates,
        carModelId: trip.metadata.selectedCar,
      });

      console.log("Danh sách driver online tạo lại trip......");
      console.log(ServiceSocket.onlineDrivers);
      console.log("------------------------------------------------------");
      let driverNearList = [];
      if (driverNearData.metadata && driverNearData?.code < 400) {
        if (driverNearData.metadata.drivers.length) {
          driverNearList = driverNearData.metadata.drivers;
          driverReceiveNoti = ServiceSocket.onlineDrivers.filter((driver) =>
            driverNearList.some(
              (item) =>
                driver.userId === item.driverId.toString() &&
                driver.status == "active" &&
                driver.userId != req.body.driverId
            )
          );
          console.log("danh sách driver sẽ nhận thông báo");
          console.log(driverReceiveNoti);
          // Gửi thông báo tới các driver qua Socket.io
          driverReceiveNoti.forEach((driver) => {
            _io.to(driver.socketId).emit("newTrip", { trip: trip.metadata });
          });
        }
      }
    }
    return res.status(200).json(trip);
  } catch (error) {
    return res.status(500).json(error);
  }
};

module.exports = {
  createTrip,
  findUserHaveTrip,
  updateTripStatus,
  acceptTrip,
  getUserTrips,
  driverCancelTrip,
};

const jwt = require("jsonwebtoken");

const verifyToken = (req, res, next) => {
  const token = req.headers.authorization;
  if (!token) {
    return res.status(401).json({
      code: 401,
      status: "error",
      message: "Require authorization",
    });
  }
  const accessToken = token.split(" ")[1];
  jwt.verify(accessToken, process.env.JWT_SECRET, (err, decode) => {
    if (err) {
      if (err instanceof jwt.TokenExpiredError) {
        return res.status(401).json({
          code: 1,
          status: "error",
          message: "Access Token Expired",
        });
      }
      return res.status(401).json({
        code: 0,
        status: "error",
        message: "Access Token Invalid",
      });
    }
    console.log(decode);
    req.user = decode.user;
    next();
  });
};

module.exports = verifyToken;

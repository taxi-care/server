const carService = require("../Services/carService");

const createCar = async (req, res) => {
  try {
    const { model_name, model_description, price } = req.body;
    if (!model_name || !model_description || !price) {
      return res.status(400).json("All fields are required");
    }
    const car = await carService.createCar(
      model_name,
      model_description,
      price
    );
    return res.status(200).json(car);
  } catch (error) {
    return res.status(500).json(error);
  }
};

const getCars = async (req, res) => {
  try {
    let cars = await carService.getCars();
    return res.status(200).json(cars);
  } catch (error) {
    return res.status(500).json(error);
  }
};

const getCarById = async (req, res) => {
  try {
    let car = await carService.getCarById(req.params);
    return res.status(200).json(car);
  } catch (error) {
    return res.status(500).json(error);
  }
};

module.exports = { createCar, getCars, getCarById };

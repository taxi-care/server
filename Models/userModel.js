const mongoose = require("mongoose");

const userSchema = new mongoose.Schema(
  {
    name: { type: String, require: true, minlength: 3, maxlength: 30 },
    password: { type: String, require: true, minlength: 3, maxlength: 1024 },
    phone: { type: Number, required: true },
    role: {
      type: String,
      enum: ["cc", "driver", "customer", "admin"],
      required: true,
      default: "customer",
    },
    license_plate: {
      type: String,
      required: false,
    },
    car_model_id: {
      type: String,
      required: false,
    },
    refresh_token: {
      type: String,
      required: false,
    },
  },
  {
    timestamps: true,
  }
);

const userModel = mongoose.model("User", userSchema);

module.exports = userModel;

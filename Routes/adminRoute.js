const express = require("express");
const adminController = require("../Controllers/adminController");
const router = express.Router();

//getAdminTrip
router.get("/trips", adminController.getAdminTrip);
router.get("/ridesStatistics", adminController.getRideStatistics);
router.get("/topDrivers", adminController.getTopDrivers);



module.exports = router;
const mongoose = require("mongoose");

const carSchema = new mongoose.Schema({
  model_name: {
    type: String,
    required: true,
  },
  model_description: {
    type: String,
    required: true,
  },
  price: {
    type: Number,
    require: true,
  },
});

const Car = mongoose.model("Car", carSchema);

module.exports = Car;

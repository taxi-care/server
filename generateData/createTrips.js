const fs = require('fs');

function randomIntFromInterval(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
}

function randomFloatFromInterval(min, max, precision = 2) {
    return parseFloat((Math.random() * (max - min) + min).toFixed(precision));
}

function generateRandomDate(month, year) {
    const date = new Date(year, month, randomIntFromInterval(1, 28), randomIntFromInterval(0, 23), randomIntFromInterval(0, 59), randomIntFromInterval(0, 59));
    return date.toISOString();
}

function generateObjectId() {
    const timestamp = ((new Date().getTime() / 1000) | 0).toString(16);
    const suffix = 'xxxxxxxxxxxxxxxx'.replace(/[x]/g, () => (Math.random() * 16 | 0).toString(16)).toLowerCase();
    return timestamp + suffix;
}

function generateRideData(month, year, userIDs, driverIDs) {
    return {
        _id: { "$oid": generateObjectId() },
        source: "Hẻm 156 Võ Văn Ngân Phường Bình Thọ, Thành Phố Thủ Đức, Thành Phố Hồ Chí Minh",
        sourceCoordinates: [106.761436, 10.84947],
        destination: "Hẻm 300 Lê Văn Việt Phường Tăng Nhơn Phú B, Thành Phố Thủ Đức, Thành Phố Hồ Chí Minh",
        destinationCoordinates: [106.785464, 10.843393],
        PhoneCustomer: "346680879",
        NameCustomer: "Anh Tài",
        distance: randomFloatFromInterval(1.0, 10.0),
        price: randomFloatFromInterval(20.0, 50.0),
        userId: { "$oid": userIDs[randomIntFromInterval(0, userIDs.length - 1)] },
        selectedCar: { "$oid": "653a6650920751bfe05a2c6e" }, // Assuming this remains constant
        driverId: { "$oid": driverIDs[randomIntFromInterval(0, driverIDs.length - 1)] },
        status: "complete",
        createdAt: { "$date": generateRandomDate(month, year) },
        updatedAt: { "$date": generateRandomDate(month, year) },
        __v: 0
    };
}

const year = 2023;
const data = [];
const userIDs = Array.from({ length: 50 }, () => generateObjectId());
const driverIDs = Array.from({ length: 10 }, () => generateObjectId());

for (let month = 0; month < 12; month++) {
    for (let i = 0; i < 5; i++) {
        data.push(generateRideData(month, year, userIDs, driverIDs));
    }
}

fs.writeFileSync('rideData.json', JSON.stringify(data, null, 2));

const tripModel = require('../Models/tripModel');

class AdminService {
    static aggregateTrip = async () => {
        try {
            const aggregatedData = await tripModel.aggregate([
                {
                    $match: { status: "complete" }
                },
                {
                    $project: {
                        monthYear: { $dateToString: { format: "%Y-%m", date: "$createdAt" } },
                        price: 1
                    }
                },
                {
                    $group: {
                        _id: "$monthYear",
                        totalRides: { $sum: 1 },
                        totalEarnings: { $sum: "$price" }
                    }
                },
                { $sort: { _id: 1 } }
            ]);
            if (aggregatedData) {
                return {
                    labels: aggregatedData.map(data => data._id),
                    rides: aggregatedData.map(data => data.totalRides),
                    earnings: aggregatedData.map(data => data.totalEarnings),
                };
            }
            return {
                code: 201,
                metadata: null,
            };
        } catch (error) {
            return {
                code: "501",
                message: error.message,
                status: "error",
            };
        }
    }
    static aggregateRideStatistics = async () => {
        try {
            const aggregatedData = await tripModel.aggregate([
                {
                    $facet: {
                        "totalRides": [{ $count: "total" }],
                        "completeRides": [{ $match: { status: "complete" } }, { $count: "total" }],
                        "numDrivers": [{ $group: { _id: "$driverId" } }, { $count: "total" }],
                        "numUsers": [{ $group: { _id: "$userId" } }, { $count: "total" }]
                    }
                },
                {
                    $project: {
                        totalRides: { $arrayElemAt: ["$totalRides.total", 0] },
                        completeRides: { $arrayElemAt: ["$completeRides.total", 0] },
                        numDrivers: { $arrayElemAt: ["$numDrivers.total", 0] },
                        numUsers: { $arrayElemAt: ["$numUsers.total", 0] }
                    }
                }
            ]);

            return aggregatedData;
        } catch (error) {
            console.error('Error aggregating ride statistics:', error);
            throw error;
        }
    }

    static getTopDriver = async () => {
        try {
            return tripModel.aggregate([
                { $match: { status: "complete" } },
                {
                    $lookup: {
                        from: "users", // Replace with the actual name of your User collection
                        localField: "driverId",
                        foreignField: "_id", // Assuming _id is the field in User collection that corresponds to driverId
                        as: "driverDetails"
                    }
                },
                { $unwind: "$driverDetails" },
                {
                    $group: {
                        _id: "$driverDetails.name", // Group by driver's name
                        totalEarnings: { $sum: "$price" }
                    }
                },
                { $sort: { totalEarnings: -1 } },
                { $limit: 3 }
            ])
        } catch (error) {
            console.error('Error aggregating top driver:', error);
            throw error;
        }
    }
}

module.exports = AdminService;